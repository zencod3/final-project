import { Briefcase, MapPin, Verified } from 'lucide-react'
import React from 'react'
import { Link } from 'react-router-dom'

export default function Card(props) {
   return (
      <div className="card card-compact w-full bg-base-100">
         <figure><img className='scale-125' src={props.company_image_url} alt={props.company_name} /></figure>
         <div className="card-body">
            <h2 className="card-title justify-between">
               <span className='font-bold text-lg lg:text-xl'>{props.title}</span>
               <span className={`badge badge-lg ${props.job_status === 1 ? 'badge-success' : 'badge-error'} text-white font-semibold`}>{props.job_status === 1 ? 'Open' : 'Close'}</span>
            </h2>
            <h3 className='flex items-center space-x-2'>
               <span className='font-bold text-base lg:text-lg'>{props.company_name}</span>
               <Verified className='w-7 h-7 fill-blue-700 text-white dark:text-base-100 font-bold' />
            </h3>
            <h4 className='flex items-center space-x-2 space-y-0'>
               <Briefcase className='w-5 h-5 text-blue-700 font-bold' />
               <span className="badge badge-md badge-info text-white font-semibold">{props.job_type}</span>
               <span className="badge badge-md badge-info text-white font-semibold">{props.job_tenure}</span>
            </h4>
            <h4 className='flex items-center space-x-2'>
               <MapPin className='w-5 h-5 text-blue-700 font-bold' />
               <span className='font-bold text-base lg:text-lg'>{props.company_city}</span>
            </h4>
            <div className="card-actions justify-end">
               <Link to={`/job-vacancy/${props.id}`} className="btn btn-primary text-white">Detail</Link>
            </div>
         </div>
      </div>
   )
}
