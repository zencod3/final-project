import React from 'react'
import { Link } from 'react-router-dom'

export default function Footer() {
   return (
      <footer className="footer p-10 lg:p-32 bg-gray-900 text-white justify-evenly">
         <nav>
            <h6 className="footer-title">Services</h6>
            <Link className="link link-hover">Branding</Link>
            <Link className="link link-hover">Design</Link>
            <Link className="link link-hover">Marketing</Link>
            <Link className="link link-hover">Advertisement</Link>
         </nav>
         <nav>
            <h6 className="footer-title">Company</h6>
            <Link className="link link-hover">About us</Link>
            <Link className="link link-hover">Contact</Link>
            <Link className="link link-hover">Jobs</Link>
            <Link className="link link-hover">Press kit</Link>
         </nav>
         <nav>
            <h6 className="footer-title">Legal</h6>
            <Link className="link link-hover">Terms of use</Link>
            <Link className="link link-hover">Privacy policy</Link>
            <Link className="link link-hover">Cookie policy</Link>
         </nav>
         <form>
            <h6 className="footer-title">Newsletter</h6>
            <fieldset className="form-control w-80">
               <label className="label">
                  <span className="label-text text-neutral-content">Enter your email address</span>
               </label>
               <div className="join">
                  <input type="text" placeholder="username@site.com" className="input input-bordered text-black join-item" />
                  <button className="btn btn-primary text-white join-item">Subscribe</button>
               </div>
            </fieldset>
         </form>
      </footer>
   )
}
