import React from 'react'

export default function Hero() {
   return (
      <div className="hero min-h-screen" style={{ backgroundImage: 'url(https://img.freepik.com/free-photo/low-angle-shot-high-rise-buildings-clear-sky-frankfurt-germany_181624-30909.jpg?w=1380&t=st=1707389988~exp=1707390588~hmac=5b6b1abcc09197bc2c56d509d28f52a6da6e098c4060fa0ed5b443f5b58c7c10)' }}>
         <div className="hero-overlay bg-opacity-60"></div>
         <div className="hero-content text-center text-white">
            <div className="max-w-md">
               <h1 className="mb-5 text-5xl font-bold">Let us assist you in discovering Your Perfect Job</h1>
               <p className="mb-5">Here, at CareerMatch, our priority is to uncover the ideal job that aligns perfectly with your best experiences.</p>
               <button className="btn btn-primary text-white">Get Started</button>
            </div>
         </div>
      </div>
   )
}
