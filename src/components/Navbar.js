import React from 'react'
import { Link } from 'react-router-dom'

export default function Navbar() {
   return (
      <div className="fixed z-10 navbar bg-gray-900 text-white">
         <div className='p-0 lg:px-64 w-full'>
            <div className="flex-none lg:hidden">
               <div className="dropdown">
                  <div tabIndex={0} role="button" className="btn btn-ghost focus:btn-outline btn-square">
                     <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h7" /></svg>
                  </div>
                  <ul tabIndex={0} className="menu menu-sm dropdown-content mt-3 z-[1] p-2 shadow bg-gray-900 text-white rounded-box w-52">
                     <li><Link to={'/'}>Homepage</Link></li>
                     <li><Link to={'/job-vacancy'}>Job Vacancy</Link></li>
                  </ul>
               </div>
            </div>
            <div className="flex-1 space-x-8">
               <Link to={'/'} className="text-xl font-bold">CareerMatch</Link>
               <Link to={'/'} className='hidden lg:inline-block'>Homepage</Link>
               <Link to={'/job-vacancy'} className='hidden lg:inline-block'>Job Vacancy</Link>
            </div>
            <div className="flex-none">
               <button className="btn btn-sm lg:btn-md btn-primary text-white">
                  Login
               </button>
            </div>
         </div>
      </div>
   )
}
