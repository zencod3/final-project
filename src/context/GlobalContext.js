import React, { createContext, useState, useContext } from 'react';

const GlobalContext = createContext();

export const useGlobalContext = () => {
   return useContext(GlobalContext);
};

export const GlobalProvider = ({ children }) => {
   const [jobs, setJobs] = useState([]);
   const [jobId, setJobId] = useState([]);
   const [loading, setLoading] = useState(false);

   const contextValue = {
      jobs,
      jobId,
      loading,
      setJobs,
      setJobId,
      setLoading,
   };

   return (
      <GlobalContext.Provider value={contextValue}>
         {children}
      </GlobalContext.Provider>
   );
};
