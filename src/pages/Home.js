import React, { useEffect } from 'react'
import Navbar from '../components/Navbar';
import Hero from '../components/Hero';
import Footer from '../components/Footer';
import { useGlobalContext } from '../context/GlobalContext';
import axios from 'axios';
import Card from '../components/Card';

export default function Home() {
  const { jobs, setJobs, loading, setLoading } = useGlobalContext()

  useEffect(() => {
    setLoading(true);
    axios.get(`https://dev-example.sanbercloud.com/api/job-vacancy`)
      .then((res) => {
        setJobs(res.data.data);
        setLoading(false);
      }).catch((err) => {
        setLoading(false);
      });
  }, [setJobs, setLoading]);

  return (
    <div>
      <Navbar />
      <Hero />
      <div className='bg-base-300'>
        <div className='px-6 py-12 lg:px-64 w-full space-y-10'>
          <h2 className='text-2xl lg:text-4xl text-center text-black dark:text-white font-bold'>Jobs Vacancy</h2>
          {loading &&
            <div className='flex justify-center'>
              <span className="loading loading-spinner loading-lg"></span>
            </div>
          }
          <div className='grid grid-cols-1 lg:grid-cols-3 gap-8'>
            {jobs && jobs.map((item, index) => (
              <Card
                company_image_url={item.company_image_url}
                title={item.title}
                job_status={item.job_status}
                company_name={item.company_name}
                job_type={item.job_type}
                job_tenure={item.job_tenure}
                company_city={item.company_city}
                id={item.id}
              />
            ))}
          </div>
        </div>
      </div>
      <Footer />
    </div>
  )
}
