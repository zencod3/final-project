import React, { useEffect, useState } from 'react'
import Navbar from '../components/Navbar'
import Footer from '../components/Footer'
import { useGlobalContext } from '../context/GlobalContext'
import axios from 'axios'
import Card from '../components/Card'

export default function About() {
   const { jobs, setJobs, loading, setLoading } = useGlobalContext()
   const [search, setSearch] = useState('')

   useEffect(() => {
      setLoading(true);
      axios.get(`https://dev-example.sanbercloud.com/api/job-vacancy`)
         .then((res) => {
            setJobs(res.data.data);
            setLoading(false);
         }).catch((err) => {
            setLoading(false);
         });
   }, [setJobs, setLoading]);

   const handleSearch = (e) => {
      setSearch(e.target.value);
   }

   const filteredData = search.length === 0 ? jobs : jobs.filter(item => (
      item.title.toLowerCase().includes(search.toLowerCase()) ||
      item.company_name.toLowerCase().includes(search.toLowerCase()) ||
      item.company_city.toLowerCase().includes(search.toLowerCase())
   ));

   return (
      <div>
         <Navbar />
         <div className='pt-16'>
            <div className='bg-base-300'>
               <div className='px-6 py-12 lg:px-64 w-full space-y-10'>
                  <div className="form-control w-full">
                     <input
                        type="text"
                        placeholder="Search"
                        value={search}
                        onChange={handleSearch}
                        className="input input-bordered focus:input-primary w-full"
                     />
                  </div>
                  {loading
                     ? (
                        <div className='flex justify-center'>
                           <span className="loading loading-spinner loading-lg"></span>
                        </div>
                     ) : filteredData.length === 0
                        ? (
                           <div className="text-center">No data found.</div>
                        ) : (
                           <div className='grid grid-cols-1 lg:grid-cols-3 gap-8'>
                              {filteredData.map((item, index) => (
                                 <div key={index}>
                                    <Card
                                       company_image_url={item.company_image_url}
                                       title={item.title}
                                       job_status={item.job_status}
                                       company_name={item.company_name}
                                       job_type={item.job_type}
                                       job_tenure={item.job_tenure}
                                       company_city={item.company_city}
                                       id={item.id}
                                    />
                                 </div>
                              ))}
                           </div>
                        )
                  }
               </div>
            </div>
         </div>
         <Footer />
      </div>
   )
}