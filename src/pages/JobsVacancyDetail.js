import React, { useEffect } from 'react';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import { useParams } from 'react-router-dom';
import { Briefcase, CircleDollarSign, MapPin, Verified } from 'lucide-react';
import axios from 'axios';
import { useGlobalContext } from '../context/GlobalContext';

export default function AboutDetail() {
   const { id } = useParams();
   const { jobId, loading, setJobId, setLoading } = useGlobalContext();

   useEffect(() => {
      setLoading(true);
      axios.get(`https://dev-example.sanbercloud.com/api/job-vacancy/${id}`)
         .then((res) => {
            setJobId(res.data);
            setLoading(false);
         }).catch((err) => {
            setLoading(false);
         });
   }, [id, setJobId, setLoading]);

   return (
      <div>
         <Navbar />
         <div className='p-4 lg:py-16 lg:px-72'>
            <div className='pt-16'>
               {loading ? (
                  <div className='flex justify-center'>
                     <span className="loading loading-spinner loading-lg"></span>
                  </div>
               ) : (
                  <div>
                     <div className='block lg:flex lg:flex-row-reverse lg:justify-between lg:items-center'>
                        <img className='w-full lg:w-1/4 aspect-auto mx-auto lg:mx-0' src={jobId?.company_image_url} alt={jobId?.company_name} />
                        <div className='p-4 space-y-4'>
                           <h2 className='text-2xl lg:text-4xl font-bold'>{jobId?.title}</h2>
                           <p className='text-lg lg:text-2xl space-x-2 space-y-2 flex font-bold items-center'>
                              <span>{jobId?.company_name}</span>
                              <Verified className='w-6 h-6 text-blue-700 font-bold' />
                              <span className={`badge badge-lg ${jobId?.job_status === 1 ? 'badge-success' : 'badge-error'} text-white font-semibold`}>{jobId?.job_status === 1 ? 'Open' : 'Close'}</span>
                           </p>
                           <p className='flex items-center space-x-2'>
                              <Briefcase className='w-5 h-5 text-blue-700 font-bold' />
                              <span className="badge badge-md badge-info text-white font-semibold">{jobId?.job_type}</span>
                              <span className="badge badge-md badge-info text-white font-semibold">{jobId?.job_tenure}</span>
                           </p>
                           <p className='flex items-center space-x-2'>
                              <MapPin className='w-5 h-5 text-blue-700 font-bold' />
                              <span className='font-bold text-base lg:text-lg'>{jobId?.company_city}</span>
                           </p>
                           <p className='flex items-center space-x-2'>
                              <CircleDollarSign className='w-5 h-5 text-blue-700 font-bold' />
                              <span className='font-bold text-base lg:text-lg'>Rp. {jobId?.salary_min} - Rp. {jobId?.salary_max}</span>
                           </p>
                        </div>
                     </div>
                     <div className='space-y-2 text-base'>
                        <p className='text-justify'><strong>Description:</strong> {jobId?.job_description}</p>
                        <p className='text-justify'><strong>Qualifications:</strong> {jobId?.job_qualification}</p>
                     </div>
                  </div>
               )}
            </div>
         </div>
         <Footer />
      </div>
   )
}
