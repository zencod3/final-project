import { createBrowserRouter } from "react-router-dom";
import Home from "./pages/Home";
import About from "./pages/JobsVacancy";
import AboutDeatil from "./pages/JobsVacancyDetail";

const routes = createBrowserRouter([
   {
      path: '/',
      element: <Home />
   },
   {
      path: '/job-vacancy',
      element: <About />
   },
   {
      path: '/job-vacancy/:id',
      element: <AboutDeatil />
   }
])

export default routes